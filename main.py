from typing import Any

import google.auth
from google.auth.transport.requests import AuthorizedSession
import requests


# Following GCP best practices, these credentials should be
# constructed at start-up time and used throughout
# https://cloud.google.com/apis/docs/client-libraries-best-practices
AUTH_SCOPE = "https://www.googleapis.com/auth/cloud-platform"
CREDENTIALS, _ = google.auth.default(scopes=[AUTH_SCOPE])


def make_composer2_web_server_request(
    url: str, method: str = "GET", json: dict = None, **kwargs: Any
) -> google.auth.transport.Response:
    """
    Make a request to Cloud Composer 2 environment's web server.
    Args:
      url: The URL to fetch.
      method: The request method to use ('GET', 'OPTIONS', 'HEAD', 'POST', 'PUT',
        'PATCH', 'DELETE')
      **kwargs: Any of the parameters defined for the request function:
                https://github.com/requests/requests/blob/master/requests/api.py
                  If no timeout is provided, it is set to 90 by default.
    """

    authed_session = AuthorizedSession(CREDENTIALS)

    # Set the default timeout, if missing
    if "timeout" not in kwargs:
        kwargs["timeout"] = 90

    return authed_session.request(method, url, json=json, **kwargs)


def call_airflow_api(request):
    """call airflow2 stable API in Composer2 Image. The request nee

    Args:
        request (_type_): _description_

    Raises:
        requests.HTTPError: _description_

    Returns:
        _type_: _description_
    """
    request_body = request.get_json()
    print(f"request_body is: {request_body}")

    default_webserver_url = "https://852ff01e734c4737b9db08eae08572ff-dot-asia-northeast1.composer.googleusercontent.com"
    webserver_url = request_body.get("webserver_url", default_webserver_url)

    method = request_body.get("method")

    # json = request_body.get(
    #     "kwargs"
    # )  # ***this is not a mistake!!! older application were using "kwargs" to pass in json body.
    # if json is None:
    #     json = request_body.get("json")
    json = (
        request_body.get("kwargs")
        or request_body.get("json")
        or request_body.get("payload")
    )
    if json is None:
        json = {}

    endpoint = request_body.get("endpoint")
    if endpoint[0] != "/":
        endpoint = "/" + endpoint
    endpoint = "/api/v1" + endpoint
    url = webserver_url + endpoint
    print(f"url - {url}")
    # Make a POST request to IAP which then Triggers the DAG
    print(f"method - {method}")
    print(f"json - {json}")
    r = make_composer2_web_server_request(url=url, method=method, json=json)

    if r.status_code == 403:
        raise requests.HTTPError(
            "You do not have a permission to perform this operation. "
            "Check Airflow RBAC roles for your account."
            f"{r.headers} / {r.text}"
        )
    elif r.status_code != 200:
        r.raise_for_status()
    else:
        return r.json()
    # print(webserver_url, method, json, sep="\n")


if __name__ == "__main__":

    url = "https://852ff01e734c4737b9db08eae08572ff-dot-asia-northeast1.composer.googleusercontent.com/dags/ecospace_digital_twin_simulation_data/dagsRun"
    method = "POST"
    payload = {}
    r = make_composer2_web_server_request(url, method, json=payload)
    print(r)
    print(r.text)
    pass
    # TODO(developer): replace with your values
    # dag_id = "your-dag-id"  # Replace with the ID of the DAG that you want to run.
    # dag_config = {
    #     "your-key": "your-value"
    # }  # Replace with configuration parameters for the DAG run.
    # # Replace web_server_url with the Airflow web server address. To obtain this
    # # URL, run the following command for your environment:
    # # gcloud composer environments describe example-environment \
    # #  --location=your-composer-region \
    # #  --format="value(config.airflowUri)"
    # web_server_url = (
    #     "https://example-airflow-ui-url-dot-us-central1.composer.googleusercontent.com"
    # )

    # response_text = trigger_dag(
    #     web_server_url=web_server_url, dag_id=dag_id, data=dag_config
    # )

    # import flask

    # req = flask.Request()
    # req.from_values(method="GET", url="https://yahoo.com.hk")
    # call_airflow_api(req)
    # pass
    # print(response_text)
